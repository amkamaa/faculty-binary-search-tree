\select@language {romanian}
\contentsline {chapter}{\numberline {1}Introducere}{4}{chapter.1}
\contentsline {section}{\numberline {1.1}Descriere general\IeC {\u a} a problemei alese}{4}{section.1.1}
\contentsline {section}{\numberline {1.2}Motivare}{5}{section.1.2}
\contentsline {section}{\numberline {1.3}Obiective}{5}{section.1.3}
\contentsline {chapter}{\numberline {2}Studiul actual \IeC {\^\i }n domeniul studiat}{7}{chapter.2}
\contentsline {chapter}{\numberline {3}Tehnologiile folosite, termenii, metodele \IeC {\textcommabelow s}i modelele folosite}{12}{chapter.3}
\contentsline {section}{\numberline {3.1}Tehnologii}{12}{section.3.1}
\contentsline {section}{\numberline {3.2}Termenii, metodele, \IeC {\textcommabelow s}i modelele folosite}{13}{section.3.2}
\contentsline {chapter}{\numberline {4}Descrierea aplica\IeC {\textcommabelow t}iei}{19}{chapter.4}
\contentsline {section}{\numberline {4.1}Exemple de utilizare}{20}{section.4.1}
\contentsline {chapter}{\numberline {5}Concluzii \IeC {\textcommabelow s}i direc\IeC {\textcommabelow t}ii viitoare}{27}{chapter.5}
\contentsline {paragraph}{Contribu\IeC {\c t}ii personale}{27}{section*.3}
\contentsline {paragraph}{Direc\IeC {\textcommabelow t}ii viitoare}{27}{section*.4}
