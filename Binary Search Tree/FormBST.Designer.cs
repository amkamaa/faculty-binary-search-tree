﻿namespace Binary_Search_Tree
{
    partial class BstVisualization
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(BstVisualization));
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.leftPanel = new System.Windows.Forms.Panel();
            this.nodesTextBox = new System.Windows.Forms.TextBox();
            this.randomTreeButton = new System.Windows.Forms.Button();
            this.randomValueButton = new System.Windows.Forms.Button();
            this.minButton = new System.Windows.Forms.Button();
            this.maxButton = new System.Windows.Forms.Button();
            this.preorderButton = new System.Windows.Forms.Button();
            this.deleteButton = new System.Windows.Forms.Button();
            this.inputValue = new System.Windows.Forms.NumericUpDown();
            this.predecessorButton = new System.Windows.Forms.Button();
            this.successorButton = new System.Windows.Forms.Button();
            this.postorderButton = new System.Windows.Forms.Button();
            this.clearButton = new System.Windows.Forms.Button();
            this.findButton = new System.Windows.Forms.Button();
            this.inorderButton = new System.Windows.Forms.Button();
            this.insertButton = new System.Windows.Forms.Button();
            this.valueLabel = new System.Windows.Forms.Label();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.rightPanelBottom = new System.Windows.Forms.Panel();
            this.consoleTextBox = new System.Windows.Forms.TextBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.pictureBox = new System.Windows.Forms.PictureBox();
            this.outputCheckBox = new System.Windows.Forms.CheckBox();
            this.tableLayoutPanel1.SuspendLayout();
            this.leftPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.inputValue)).BeginInit();
            this.tableLayoutPanel2.SuspendLayout();
            this.rightPanelBottom.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox)).BeginInit();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 197F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.leftPanel, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel2, 1, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(834, 531);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // leftPanel
            // 
            this.leftPanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.leftPanel.Controls.Add(this.outputCheckBox);
            this.leftPanel.Controls.Add(this.nodesTextBox);
            this.leftPanel.Controls.Add(this.randomTreeButton);
            this.leftPanel.Controls.Add(this.randomValueButton);
            this.leftPanel.Controls.Add(this.minButton);
            this.leftPanel.Controls.Add(this.maxButton);
            this.leftPanel.Controls.Add(this.preorderButton);
            this.leftPanel.Controls.Add(this.deleteButton);
            this.leftPanel.Controls.Add(this.inputValue);
            this.leftPanel.Controls.Add(this.predecessorButton);
            this.leftPanel.Controls.Add(this.successorButton);
            this.leftPanel.Controls.Add(this.postorderButton);
            this.leftPanel.Controls.Add(this.clearButton);
            this.leftPanel.Controls.Add(this.findButton);
            this.leftPanel.Controls.Add(this.inorderButton);
            this.leftPanel.Controls.Add(this.insertButton);
            this.leftPanel.Controls.Add(this.valueLabel);
            this.leftPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.leftPanel.Location = new System.Drawing.Point(6, 6);
            this.leftPanel.Margin = new System.Windows.Forms.Padding(6, 6, 3, 6);
            this.leftPanel.Name = "leftPanel";
            this.leftPanel.Size = new System.Drawing.Size(188, 519);
            this.leftPanel.TabIndex = 0;
            // 
            // nodesTextBox
            // 
            this.nodesTextBox.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.nodesTextBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.nodesTextBox.CausesValidation = false;
            this.nodesTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nodesTextBox.HideSelection = false;
            this.nodesTextBox.Location = new System.Drawing.Point(43, 497);
            this.nodesTextBox.MaxLength = 12;
            this.nodesTextBox.Name = "nodesTextBox";
            this.nodesTextBox.ReadOnly = true;
            this.nodesTextBox.ShortcutsEnabled = false;
            this.nodesTextBox.Size = new System.Drawing.Size(100, 14);
            this.nodesTextBox.TabIndex = 1;
            this.nodesTextBox.Text = "Nodes: 0";
            this.nodesTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // randomTreeButton
            // 
            this.randomTreeButton.BackgroundImage = global::Binary_Search_Tree.Properties.Resources.tree;
            this.randomTreeButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.randomTreeButton.Location = new System.Drawing.Point(160, 8);
            this.randomTreeButton.Name = "randomTreeButton";
            this.randomTreeButton.Size = new System.Drawing.Size(22, 22);
            this.randomTreeButton.TabIndex = 5;
            this.randomTreeButton.UseVisualStyleBackColor = true;
            this.randomTreeButton.Click += new System.EventHandler(this.RandomTreeButton_Click);
            // 
            // randomValueButton
            // 
            this.randomValueButton.BackgroundImage = global::Binary_Search_Tree.Properties.Resources.refresh;
            this.randomValueButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.randomValueButton.Location = new System.Drawing.Point(134, 8);
            this.randomValueButton.Name = "randomValueButton";
            this.randomValueButton.Size = new System.Drawing.Size(22, 22);
            this.randomValueButton.TabIndex = 5;
            this.randomValueButton.UseVisualStyleBackColor = true;
            this.randomValueButton.Click += new System.EventHandler(this.RandomValueButton_Click);
            // 
            // minButton
            // 
            this.minButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.minButton.Location = new System.Drawing.Point(4, 195);
            this.minButton.Name = "minButton";
            this.minButton.Size = new System.Drawing.Size(84, 23);
            this.minButton.TabIndex = 2;
            this.minButton.Text = "Minimum";
            this.minButton.UseVisualStyleBackColor = true;
            this.minButton.Click += new System.EventHandler(this.MinButton_Click);
            // 
            // maxButton
            // 
            this.maxButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.maxButton.Location = new System.Drawing.Point(98, 195);
            this.maxButton.Name = "maxButton";
            this.maxButton.Size = new System.Drawing.Size(84, 23);
            this.maxButton.TabIndex = 2;
            this.maxButton.Text = "Maximum";
            this.maxButton.UseVisualStyleBackColor = true;
            this.maxButton.Click += new System.EventHandler(this.MaxButton_Click);
            // 
            // preorderButton
            // 
            this.preorderButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.preorderButton.Location = new System.Drawing.Point(98, 224);
            this.preorderButton.Name = "preorderButton";
            this.preorderButton.Size = new System.Drawing.Size(84, 23);
            this.preorderButton.TabIndex = 2;
            this.preorderButton.Text = "Preorder";
            this.preorderButton.UseVisualStyleBackColor = true;
            this.preorderButton.Click += new System.EventHandler(this.PreorderButton_Click);
            // 
            // deleteButton
            // 
            this.deleteButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.deleteButton.Location = new System.Drawing.Point(98, 41);
            this.deleteButton.Name = "deleteButton";
            this.deleteButton.Size = new System.Drawing.Size(84, 23);
            this.deleteButton.TabIndex = 2;
            this.deleteButton.Text = "Delete";
            this.deleteButton.UseVisualStyleBackColor = true;
            this.deleteButton.Click += new System.EventHandler(this.DeleteButton_Click);
            // 
            // inputValue
            // 
            this.inputValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.inputValue.Location = new System.Drawing.Point(53, 9);
            this.inputValue.Maximum = new decimal(new int[] {
            999,
            0,
            0,
            0});
            this.inputValue.Name = "inputValue";
            this.inputValue.Size = new System.Drawing.Size(75, 21);
            this.inputValue.TabIndex = 3;
            this.inputValue.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // predecessorButton
            // 
            this.predecessorButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.predecessorButton.Location = new System.Drawing.Point(98, 99);
            this.predecessorButton.Name = "predecessorButton";
            this.predecessorButton.Size = new System.Drawing.Size(84, 23);
            this.predecessorButton.TabIndex = 2;
            this.predecessorButton.Text = "Predecessor";
            this.predecessorButton.UseVisualStyleBackColor = true;
            this.predecessorButton.Click += new System.EventHandler(this.predecessorButton_Click);
            // 
            // successorButton
            // 
            this.successorButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.successorButton.Location = new System.Drawing.Point(4, 99);
            this.successorButton.Name = "successorButton";
            this.successorButton.Size = new System.Drawing.Size(84, 23);
            this.successorButton.TabIndex = 2;
            this.successorButton.Text = "Successor";
            this.successorButton.UseVisualStyleBackColor = true;
            this.successorButton.Click += new System.EventHandler(this.successorButton_Click);
            // 
            // postorderButton
            // 
            this.postorderButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.postorderButton.Location = new System.Drawing.Point(51, 253);
            this.postorderButton.Name = "postorderButton";
            this.postorderButton.Size = new System.Drawing.Size(84, 23);
            this.postorderButton.TabIndex = 2;
            this.postorderButton.Text = "Postorder";
            this.postorderButton.UseVisualStyleBackColor = true;
            this.postorderButton.Click += new System.EventHandler(this.PostorderButton_Click);
            // 
            // clearButton
            // 
            this.clearButton.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.clearButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.clearButton.Location = new System.Drawing.Point(58, 425);
            this.clearButton.Name = "clearButton";
            this.clearButton.Size = new System.Drawing.Size(71, 47);
            this.clearButton.TabIndex = 2;
            this.clearButton.Text = "Clear";
            this.clearButton.UseVisualStyleBackColor = true;
            this.clearButton.Click += new System.EventHandler(this.ClearButton_Click);
            // 
            // findButton
            // 
            this.findButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.findButton.Location = new System.Drawing.Point(51, 70);
            this.findButton.Name = "findButton";
            this.findButton.Size = new System.Drawing.Size(84, 23);
            this.findButton.TabIndex = 2;
            this.findButton.Text = "Find";
            this.findButton.UseVisualStyleBackColor = true;
            this.findButton.Click += new System.EventHandler(this.FindButton_Click);
            // 
            // inorderButton
            // 
            this.inorderButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.inorderButton.Location = new System.Drawing.Point(4, 224);
            this.inorderButton.Name = "inorderButton";
            this.inorderButton.Size = new System.Drawing.Size(84, 23);
            this.inorderButton.TabIndex = 2;
            this.inorderButton.Text = "Inorder";
            this.inorderButton.UseVisualStyleBackColor = true;
            this.inorderButton.Click += new System.EventHandler(this.InorderButton_Click);
            // 
            // insertButton
            // 
            this.insertButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.insertButton.Location = new System.Drawing.Point(4, 41);
            this.insertButton.Name = "insertButton";
            this.insertButton.Size = new System.Drawing.Size(84, 23);
            this.insertButton.TabIndex = 2;
            this.insertButton.Text = "Insert";
            this.insertButton.UseVisualStyleBackColor = true;
            this.insertButton.Click += new System.EventHandler(this.InsertButton_Click);
            // 
            // valueLabel
            // 
            this.valueLabel.AutoSize = true;
            this.valueLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.valueLabel.Location = new System.Drawing.Point(9, 11);
            this.valueLabel.Name = "valueLabel";
            this.valueLabel.Size = new System.Drawing.Size(41, 15);
            this.valueLabel.TabIndex = 1;
            this.valueLabel.Text = "Value:";
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.BackColor = System.Drawing.SystemColors.Control;
            this.tableLayoutPanel2.ColumnCount = 1;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.Controls.Add(this.rightPanelBottom, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.panel1, 0, 0);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(197, 3);
            this.tableLayoutPanel2.Margin = new System.Windows.Forms.Padding(0, 3, 3, 3);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 2;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 72.95238F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 27.04762F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(634, 525);
            this.tableLayoutPanel2.TabIndex = 1;
            // 
            // rightPanelBottom
            // 
            this.rightPanelBottom.Controls.Add(this.consoleTextBox);
            this.rightPanelBottom.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rightPanelBottom.Location = new System.Drawing.Point(3, 385);
            this.rightPanelBottom.Name = "rightPanelBottom";
            this.rightPanelBottom.Size = new System.Drawing.Size(628, 137);
            this.rightPanelBottom.TabIndex = 0;
            // 
            // consoleTextBox
            // 
            this.consoleTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.consoleTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.consoleTextBox.Location = new System.Drawing.Point(0, 0);
            this.consoleTextBox.Margin = new System.Windows.Forms.Padding(0);
            this.consoleTextBox.Multiline = true;
            this.consoleTextBox.Name = "consoleTextBox";
            this.consoleTextBox.ReadOnly = true;
            this.consoleTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.consoleTextBox.Size = new System.Drawing.Size(628, 137);
            this.consoleTextBox.TabIndex = 0;
            // 
            // panel1
            // 
            this.panel1.AutoScroll = true;
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.pictureBox);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(3, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(628, 376);
            this.panel1.TabIndex = 1;
            // 
            // pictureBox
            // 
            this.pictureBox.BackColor = System.Drawing.Color.White;
            this.pictureBox.Location = new System.Drawing.Point(0, 0);
            this.pictureBox.MinimumSize = new System.Drawing.Size(20000, 5000);
            this.pictureBox.Name = "pictureBox";
            this.pictureBox.Size = new System.Drawing.Size(20000, 5000);
            this.pictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox.TabIndex = 0;
            this.pictureBox.TabStop = false;
            this.pictureBox.Paint += new System.Windows.Forms.PaintEventHandler(this.PictureBox_Paint);
            // 
            // outputCheckBox
            // 
            this.outputCheckBox.AutoSize = true;
            this.outputCheckBox.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.outputCheckBox.Checked = true;
            this.outputCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.outputCheckBox.Location = new System.Drawing.Point(63, 172);
            this.outputCheckBox.Name = "outputCheckBox";
            this.outputCheckBox.Size = new System.Drawing.Size(61, 17);
            this.outputCheckBox.TabIndex = 6;
            this.outputCheckBox.Text = "Output:";
            this.outputCheckBox.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.outputCheckBox.UseVisualStyleBackColor = true;
            this.outputCheckBox.CheckedChanged += new System.EventHandler(this.outputCheckBox_CheckedChanged);
            // 
            // BstVisualization
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.ClientSize = new System.Drawing.Size(834, 531);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Cursor = System.Windows.Forms.Cursors.Default;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimumSize = new System.Drawing.Size(850, 570);
            this.Name = "BstVisualization";
            this.Text = "BST Visualization";
            this.Load += new System.EventHandler(this.BSTVisualization_Load);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.leftPanel.ResumeLayout(false);
            this.leftPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.inputValue)).EndInit();
            this.tableLayoutPanel2.ResumeLayout(false);
            this.rightPanelBottom.ResumeLayout(false);
            this.rightPanelBottom.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Panel leftPanel;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.Panel rightPanelBottom;
        private System.Windows.Forms.TextBox consoleTextBox;
        private System.Windows.Forms.Label valueLabel;
        private System.Windows.Forms.Button deleteButton;
        private System.Windows.Forms.Button insertButton;
        private System.Windows.Forms.NumericUpDown inputValue;
        private System.Windows.Forms.Button findButton;
        private System.Windows.Forms.Button predecessorButton;
        private System.Windows.Forms.Button minButton;
        private System.Windows.Forms.Button successorButton;
        private System.Windows.Forms.Button maxButton;
        private System.Windows.Forms.Button preorderButton;
        private System.Windows.Forms.Button postorderButton;
        private System.Windows.Forms.Button clearButton;
        private System.Windows.Forms.Button inorderButton;
        private System.Windows.Forms.Button randomValueButton;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.PictureBox pictureBox;
        private System.Windows.Forms.TextBox nodesTextBox;
        private System.Windows.Forms.Button randomTreeButton;
        private System.Windows.Forms.CheckBox outputCheckBox;
    }
}

