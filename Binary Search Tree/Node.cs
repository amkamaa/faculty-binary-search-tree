﻿using System.Drawing;

namespace Binary_Search_Tree
{
    internal class Node
    {
        public Node(int key)
        {
            Key = key;
        }

        internal int Key { get; set; }

        internal Node Left { get; set; }

        internal Node Right { get; set; }

        internal Node Parent { get; set; }

        public Point Point { get; set; }

        public Node GetMinimumValueOfSubtree()
        {
            return Left == null ? this : Left.GetMinimumValueOfSubtree();
        }

        public Node GetMaximumValueOfSubtree()
        {
            return Right == null ? this : Right.GetMaximumValueOfSubtree();
        }

        public Node GetBiggerParent() //gaseste cel mai mare copil care e copil de dreapta
        {
            if (Parent == null)
                return null;

            var current = this;
            var tmp = Parent;

            while (tmp != null && current.Key < tmp.Key)
            {
                current = tmp;
                tmp = tmp.Parent;
            }

            return current;
        }

        public Node GetLowerParent() //cea mai mare valoare mai mica decat el
        {
            if (Parent == null)
                return null;

            var current = this;
            var tmp = Parent;

            while (tmp != null && tmp.Key < current.Key)
            {
                current = tmp;
                tmp = tmp.Parent;
            }

            return current;
        }

        public bool IsLeaf()
        {
            return Left == null && Right == null;
        }

        public override string ToString()
        {
            return "[key=" + Key + ", " +
                   "parent=" + (Parent?.Key.ToString() ?? "-") + ", " +
                   "l=" + (Left?.ToString() ?? "-") + ", " +
                   "r=" + (Right?.ToString() ?? "-") + "]";
        }
    }
}
