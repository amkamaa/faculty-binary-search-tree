﻿using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.Windows.Forms;

namespace Binary_Search_Tree
{
    class Draw
    {
        private readonly DrawNode _drawNode;
        private readonly DrawEdge _drawEdge;
        private readonly Bitmap _image;

        public Draw(PictureBox pictureBox)
        {
            _image = new Bitmap(pictureBox.ClientSize.Width, pictureBox.ClientSize.Height, PixelFormat.Format32bppArgb);
            var drawingArea = Graphics.FromImage(_image);
            drawingArea.SmoothingMode = SmoothingMode.AntiAlias;

            _drawNode = new DrawNode(drawingArea);
            _drawEdge = new DrawEdge(drawingArea);
        }
        public Bitmap Image => _image;

        public void Add(Node node)
        {
            if (node == null) return;
            _drawNode.Add(node);
        }

        public void Delete(Node node)
        {
            if (node == null) return;
            _drawNode.Remove(node);
        }

        public void Mark(Node node)
        {
            if (node == null) return;
            _drawNode.Mark(node);
        }

        public void Unmark(Node node)
        {
            if (node == null) return;
            _drawNode.Unmark(node);
        }

        public void AddEdge(Node node1, Node node2)
        {
            if (node1 == null || node2 == null) return;
            _drawEdge.Add(node1, node2);
        }

        public void DeleteEdge(Node node1, Node node2)
        {
            if (node1 == null || node2 == null) return;
            _drawEdge.Remove(node1, node2);
        }

        public void MoveSubtree(Node node, int deltaX, int deltaY)
        {
            if (node == null)
                return;

            PostorderMove(node, deltaX, deltaY);

            if (node.Parent != null)
                AddEdge(node.Parent, node);
        }

        public void MoveRightSubtree(Node node)
        {
            var lowParent = node.GetLowerParent();
            var bigParent = lowParent.GetBiggerParent();

            while (bigParent != null)
            {
                MoveSubtree(bigParent, 26, 0);
                MoveSubtree(lowParent, -26, 0);
                lowParent = bigParent.GetLowerParent();
                bigParent = lowParent?.GetBiggerParent();
            }
        }

        public void MoveLeftSubtree(Node node)
        {
            var lowParent = node.GetLowerParent();
            var bigParent = node.GetBiggerParent();

            while (bigParent != null)
            {
                MoveSubtree(bigParent, 26, 0);
                if (lowParent != node)
                    MoveSubtree(lowParent, -26, 0);
                lowParent = bigParent.GetLowerParent();
                bigParent = lowParent?.GetBiggerParent();
            }
        }

        private void PostorderMove(Node node, int deltaX, int deltaY)
        {
            if (node == null) return;

            PostorderMove(node.Left, deltaX, deltaY);
            PostorderMove(node.Right, deltaX, deltaY);
            Move(node, deltaX, deltaY);
        }

        public void Move(Node node, int deltaX, int deltaY)
        {
            if (node == null)
                return;

            Delete(node);
            if (node.Parent != null)
                DeleteEdge(node.Parent, node);

            node.Point = new Point(node.Point.X + deltaX, node.Point.Y + deltaY);

            AddEdge(node, node.Left);
            AddEdge(node, node.Right);
            Add(node);
        }
    }
}