﻿using System.Drawing;

namespace Binary_Search_Tree
{
    internal class DrawEdge
    {
        private readonly Graphics _graphics;
        private readonly Pen _blackPen = new Pen(Color.Black);
        private readonly Pen _whitePen = new Pen(Color.White, 3);

        public DrawEdge(Graphics graphics)
        {
            _graphics = graphics;
        }

        public void Add(Node node1, Node node2)
        {
            _graphics.DrawLine(_blackPen, node1.Point.X + 13, node1.Point.Y + 26, node2.Point.X + 13, node2.Point.Y - 1);
        }

        public void Remove(Node node1, Node node2)
        {
            _graphics.DrawLine(_whitePen, node1.Point.X + 13, node1.Point.Y + 26, node2.Point.X + 13, node2.Point.Y - 1);
        }
    }
}
