﻿using System;
using System.Windows.Forms;

namespace Binary_Search_Tree
{
    class Console
    {
        private TextBox _textBox;

        public Console(TextBox textBox)
        {
            _textBox = textBox;
        }

        public void Write(string text)
        {
            _textBox.AppendText(text);
        }

        public void WriteLine(string text)
        {
            Write(text);
            _textBox.AppendText(Environment.NewLine);
        }

        public void WriteLine()
        {
            _textBox.AppendText(Environment.NewLine);
        }

        public void WriteSteps(int key, int step, Node node)
        {
            Write("[STEP " + step + "]: " + node.Key);
            if (key == node.Key)
            {
                Write(" = " + key + ".");
            }
            else
            {
                Write(key > node.Key ? " < " : " > ");
                Write(key + ". Go to " + (key < node.Key ? "left" : "right") + " node.");
            }
            WriteLine();
        }
    }
}
