﻿using System.Drawing;

namespace Binary_Search_Tree
{
    class DrawNode
    {
        private readonly Graphics _graphics;
        private readonly SolidBrush _blackBrush = new SolidBrush(Color.Black);
        private readonly SolidBrush _whiteBrush = new SolidBrush(Color.White);
        private readonly SolidBrush _greyBrush = new SolidBrush(Color.WhiteSmoke);
        private readonly Pen _blackPen = new Pen(Color.Black);
        private readonly Pen _whitePen = new Pen(Color.White);
        private readonly Font _font = new Font("Consolas", 8);

        public DrawNode(Graphics graphics)
        {
            _graphics = graphics;
        }

        public void Add(Node node)
        {
            _graphics.FillEllipse(_greyBrush, node.Point.X, node.Point.Y, 25, 25);
            _graphics.DrawEllipse(_blackPen, node.Point.X, node.Point.Y, 25, 25);
            _graphics.DrawString(node.Key.ToString(), _font, _blackBrush,
                node.Point.X + StringOffsetSize(node.Key.ToString()), node.Point.Y + 6);
        }

        public void Mark(Node node)
        {
            _graphics.FillEllipse(new SolidBrush(Color.Chartreuse), node.Point.X, node.Point.Y, 25, 25);
            _graphics.DrawEllipse(_blackPen, node.Point.X, node.Point.Y, 25, 25);
            _graphics.DrawString(node.Key.ToString(), _font, _blackBrush,
                node.Point.X + StringOffsetSize(node.Key.ToString()), node.Point.Y + 6);
        }

        public void Unmark(Node node)
        {
            Add(node);
        }

        public void Remove(Node node)
        {
            _graphics.FillEllipse(_whiteBrush, node.Point.X - 1, node.Point.Y - 1, 27, 27);
            _graphics.DrawEllipse(_whitePen, node.Point.X, node.Point.Y, 25, 25);
        }

        private static int StringOffsetSize(string value)
        {
            if (value.Length == 1)
                return 8;
            return value.Length == 2 ? 5 : 2;
        }
    }
}