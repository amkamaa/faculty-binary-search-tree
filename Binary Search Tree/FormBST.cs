﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

namespace Binary_Search_Tree
{
    public partial class BstVisualization : Form
    {
        private Bst _bst;

        public BstVisualization()
        {
            InitializeComponent();
        }

        private void BSTVisualization_Load(object sender, EventArgs e)
        {
            InitializeBst();
            GenerateRandomBst();
            inputValue.Select();
        }

        private void GenerateRandomBst()
        {
            _bst.RandomWrite = true;
            var rnd = new Random();
            var min = decimal.ToInt32(inputValue.Minimum);
            var max = decimal.ToInt32(inputValue.Maximum);

            for (int i = 0; i < rnd.Next(min, max / 2); i++)
            {
                _bst.Add(rnd.Next(min, max));
            }

            _bst.RandomWrite = false;
        }

        private void InitializeBst()
        {
            _bst = new Bst(pictureBox)
            {
                Console = new Console(consoleTextBox),
                Label = nodesTextBox,
                Write = outputCheckBox.Checked
            };
        }

        private void MinButton_Click(object sender, EventArgs e)
        {
            _bst.GetMinValue();
        }

        private void MaxButton_Click(object sender, EventArgs e)
        {
            _bst.GetMaxValue();
        }

        private void InsertButton_Click(object sender, EventArgs e)
        {
            _bst.Add(Convert.ToInt32(inputValue.Text));
        }

        private void InorderButton_Click(object sender, EventArgs e)
        {
            _bst.Inorder();
        }

        private void ClearButton_Click(object sender, EventArgs e)
        {
            nodesTextBox.Text = @"Nodes: 0";
            consoleTextBox.Text = "";
            Graphics.FromImage(_bst.Bitmap);

            InitializeBst();
        }

        private void PreorderButton_Click(object sender, EventArgs e)
        {
            _bst.Preorder();
        }

        private void PostorderButton_Click(object sender, EventArgs e)
        {
            _bst.Postorder();
        }

        private void PictureBox_Paint(object sender, PaintEventArgs e)
        {
            e.Graphics.DrawImage(_bst.Bitmap, 0, 0, _bst.Bitmap.Width, _bst.Bitmap.Height);
            pictureBox.Invalidate(); //face un refresh la desen
        }

        private void DeleteButton_Click(object sender, EventArgs e)
        {
            _bst.Delete(Convert.ToInt32(inputValue.Text));
        }

        private void FindButton_Click(object sender, EventArgs e)
        {
            _bst.Find(Convert.ToInt32(inputValue.Text));
        }

        private void RandomValueButton_Click(object sender, EventArgs e)
        {
            var rnd = new Random();

            inputValue.Text = rnd.Next(decimal.ToInt32(inputValue.Minimum),
                decimal.ToInt32(inputValue.Maximum)).ToString();
        }

        private void RandomTreeButton_Click(object sender, EventArgs e)
        {
            GenerateRandomBst();
        }

        private void successorButton_Click(object sender, EventArgs e)
        {
            _bst.Successor(Convert.ToInt32(inputValue.Text));
        }

        private void predecessorButton_Click(object sender, EventArgs e)
        {
            _bst.Predecessor(Convert.ToInt32(inputValue.Text));
        }

        private void outputCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            _bst.Write = outputCheckBox.Checked;
        }
    }
}