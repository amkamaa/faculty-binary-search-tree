﻿using System.Drawing;
using System.Threading;
using System.Windows.Forms;

namespace Binary_Search_Tree
{
    internal class Bst
    {
        private readonly Draw _draw;
        private Console _out;
        private bool _writePermissions;
        private bool _isRandomWrite;
        private TextBox _nodesBox;
        private Node _root;
        private int _nodes;
        private const int WaitTime = 1000;

        public Bst(PictureBox pictureBox)
        {
            _draw = new Draw(pictureBox);
            _root = null;
            _writePermissions = true;
            _isRandomWrite = false;
            _nodes = 0;
        }

        public Console Console
        {
            set => _out = value;
        }

        public TextBox Label
        {
            set => _nodesBox = value;
        }

        public bool Write
        {
            set => _writePermissions = value;
        }

        public bool RandomWrite
        {
            set => _isRandomWrite = value;
        }

        public int Nodes => _nodes;
        public Bitmap Bitmap => _draw.Image;

        private bool Exists(int key)
        {
            return GetNode(key) != null;
        }

        public void Add(int key)
        {
            if (_root == null)
            {
                _root = new Node(key) {Point = new Point(26, 26), Parent = null};
                _draw.Add(_root);

                if (!_isRandomWrite)
                {
                    if(_writePermissions)
                        _out.WriteLine("[STEP 1]: Root doesn't exist. " + key + " is now root!");
                    _out.WriteLine("Key " + key + " was added!");
                    _out.WriteLine();
                }
            }
            else
            {
                if (Exists(key))
                {
                    if (_isRandomWrite) return;

                    _out.WriteLine("Key " + key + " exists!");
                    _out.WriteLine();
                    return;
                }

                var parent = GetParent(key);
                var newNode = new Node(key) {Parent = parent};

                if (newNode.Key < parent.Key)
                {
                    newNode.Point = new Point(parent.Point.X - 26, parent.Point.Y + 35);
                    parent.Left = newNode;

                    _draw.MoveLeftSubtree(newNode);
                }
                else
                {
                    newNode.Point = new Point(parent.Point.X + 26, parent.Point.Y + 35);
                    parent.Right = newNode;

                    _draw.MoveRightSubtree(newNode);
                }

                _draw.AddEdge(parent, newNode);
                _draw.Add(newNode);

                if (!_isRandomWrite)
                {
                    _out.WriteLine("Key " + key + " was added!");
                    _out.WriteLine();
                }
            }

            _nodes++;
            UpdateNodesLabel();
        }

        private Node GetParent(int key)
        {
            var current = _root;
            var next = current;
            
            if (!_isRandomWrite && _writePermissions)
                DrawPathTo(key);

            while (next != null)
            {
                current = next;
                next = key < current.Key ? next.Left : next.Right;
            }

            return current;
        }

        private void DrawPathTo(int key)
        {
            if (_root == null)
                return;

            var step = 1;
            var current = _root;
            var next = current;

            _draw.Mark(_root);
            var thread = new Thread(() => UnmarkThread(_draw, _root));

            _out.WriteSteps(key, step++, _root);

            thread.Start();
            thread.Join();

            while (next != null)
            {
                current = next;
                next = key < current.Key ? next.Left : next.Right;

                if (next != null)
                {
                    _draw.Mark(next);
                    thread = new Thread(() => UnmarkThread(_draw, next));

                    _out.WriteSteps(key, step++, next);
                    thread.Start();
                    thread.Join();

                    if (next.Key == key)
                        return;
                }
            }
        }

        public Node GetNode(int key)
        {
            var tmp = _root;

            while (tmp != null && tmp.Key != key)
                tmp = key < tmp.Key ? tmp.Left : tmp.Right;

            return tmp;
        }

        public void Find(int key)
        {
            var node = GetNode(key);
            
            if (node == null)
            {
                _out.WriteLine("Key " + key + " was not found!");
                _out.WriteLine();
                return;
            }

            if (!_writePermissions)
            {
                _draw.Mark(node);
                var thread = new Thread(() => UnmarkThread(_draw, node));
                thread.Start();
                thread.Join();
            }
            else
            {
                DrawPathTo(key);
            }

            _out.WriteLine("Key " + key + " was found!");
            _out.WriteLine();
        }

        public void Delete(int key)
        {
            var node = GetNode(key);

            if (node == null)
            {
                _out.WriteLine("Key " + key + " was not found!");
                _out.WriteLine();
                return;
            }

            if(_writePermissions)
                DrawPathTo(key);
            Delete(node);

            _out.WriteLine("Key " + key + " was removed!");
            _out.WriteLine();

            _nodes--;
            UpdateNodesLabel();
        }

        private void Delete(Node node)
        {
            if (node.IsLeaf())
                DeleteNodeWithoutChilds(node);
            if (node.Left == null && node.Right != null)
                DeleteNodeWithRightChild(node);
            else if (node.Left != null && node.Right == null)
                DeleteNodeWithLeftChild(node);
            else
            {
                var maximum = node.Left?.GetMaximumValueOfSubtree();

                if (maximum == null) return;

                node.Key = maximum.Key;
                _draw.Add(node);

                if (maximum.IsLeaf())
                    DeleteNodeWithoutChilds(maximum);
                else
                    Delete(maximum);
            }
        }

        private void DeleteNodeWithoutChilds(Node node)
        {
            if (node.Parent != null)
            {
                _draw.DeleteEdge(node.Parent, node);

                if (node.Key <= node.Parent.Key)
                    node.Parent.Left = null;
                else
                    node.Parent.Right = null;
            }
            else
                _root = null;

            _draw.Delete(node);
        }

        private void DeleteNodeWithRightChild(Node node)
        {
            _draw.Delete(node);
            _draw.DeleteEdge(node.Parent, node);
            _draw.DeleteEdge(node, node.Right);
            SetParentNewChild(node, node.Right);
            _draw.MoveSubtree(node.Right, 0, -35);

            if (_root == node)
                _root = node.Right;
        }

        private void DeleteNodeWithLeftChild(Node node)
        {
            _draw.Delete(node);
            _draw.DeleteEdge(node.Parent, node);
            _draw.DeleteEdge(node, node.Left);
            SetParentNewChild(node, node.Left);
            _draw.MoveSubtree(node.Left, 0, -35);

            if (_root == node)
                _root = node.Left;
        }

        private void SetParentNewChild(Node deleteNode, Node newNode)
        {
            newNode.Parent = deleteNode.Parent;

            if (deleteNode.Parent == null)
                return;

            if (deleteNode.Parent.Key < deleteNode.Key)
                deleteNode.Parent.Right = newNode;
            else
                deleteNode.Parent.Left = newNode;
        }

        public void GetMinValue()
        {
            if (_root == null)
                return;

            var tmp = _root;
            var step = 1;
            Thread thread;

            if (_writePermissions)
            {
                _draw.Mark(_root);
                thread = new Thread(() => UnmarkThread(_draw, _root));
                thread.Start();
                thread.Join();
            }

            while (tmp.Left != null)
            {
                tmp = tmp.Left;

                if (_writePermissions)
                {
                    _draw.Mark(tmp);
                    thread = new Thread(() => UnmarkThread(_draw, tmp));
                    _out.WriteLine("[STEP " + step++ + "]: " + tmp.Parent.Key + " > " + tmp.Key + ". Go to left child!");
                    thread.Start();
                    thread.Join();
                }
            }

            _out.WriteLine("Minimum value is " + tmp.Key + ".");
            _out.WriteLine();

            if (!_writePermissions)
            {
                _draw.Mark(tmp);
                thread = new Thread(() => UnmarkThread(_draw, tmp));
                thread.Start();
                thread.Join();
            }
        }

        public void GetMaxValue()
        {
            if (_root == null)
                return;

            var tmp = _root;
            var step = 1;
            Thread thread;
            if (_writePermissions)
            {
                _draw.Mark(_root);
                thread = new Thread(() => UnmarkThread(_draw, _root));
                thread.Start();
                thread.Join();
            }

            while (tmp.Right != null)
            {
                tmp = tmp.Right;

                if (_writePermissions)
                {
                    _draw.Mark(tmp);
                    thread = new Thread(() => UnmarkThread(_draw, tmp));
                    _out.WriteLine("[STEP " + step++ + "]: " + tmp.Parent.Key + " < " + tmp.Key + ". Go to right child!");
                    thread.Start();
                    thread.Join();
                }
            }

            _out.WriteLine("Maximum value is " + tmp.Key + ".");
            _out.WriteLine();

            if (!_writePermissions)
            {
                _draw.Mark(tmp);
                thread = new Thread(() => UnmarkThread(_draw, tmp));
                thread.Start();
                thread.Join();
            }
        }

        private static void UnmarkThread(Draw draw, Node node)
        {
            Thread.Sleep(WaitTime);
            draw.Unmark(node);
        }

        public void Inorder()
        {
            if (Nodes == 0)
                return;

            var message = "Inorder traversal: ";
            Inorder(_root, ref message);
            _out.WriteLine(message.Substring(0, message.Length - 2) + ".");
        }

        private void Inorder(Node node, ref string result)
        {
            if (node == null) return;

            Inorder(node.Left, ref result);
            result += node.Key + @", ";
            Inorder(node.Right, ref result);
        }

        public void Postorder()
        {
            if (Nodes == 0)
                return;

            var message = "Postorder traversal: ";
            Postorder(_root, ref message);
            _out.WriteLine(message.Substring(0, message.Length - 2) + ".");
        }

        private void Postorder(Node node, ref string result)
        {
            if (node == null) return;

            Postorder(node.Left, ref result);
            Postorder(node.Right, ref result);
            result += node.Key + @", ";
        }

        public void Preorder()
        {
            if (Nodes == 0)
                return;

            var message = "Preorder traversal: ";
            Preorder(_root, ref message);
            _out.WriteLine(message.Substring(0, message.Length - 2) + ".");
        }

        private void Preorder(Node node, ref string result)
        {
            if (node == null) return;

            result += node.Key + @", ";
            Postorder(node.Left, ref result);
            Postorder(node.Right, ref result);
        }

        public void Successor(int key)
        {
            var current = GetNode(key);

            if (current == null)
            {
                _out.WriteLine("Key " + key + " was not found!");
                _out.WriteLine();
                return;
            }

            if (current.Right != null)
            {
                var maximum = current.Right.GetMaximumValueOfSubtree();
                _out.WriteLine("The successor of node " + key + " is " + maximum.Key + ".");
                _draw.Mark(maximum);
                var thread = new Thread(() => UnmarkThread(_draw, maximum));
                thread.Start();
                thread.Join();
            }
            else
            {
                var tmp = current.Parent;

                while (tmp != null && current == tmp.Right)
                {
                    current = tmp;
                    tmp = tmp.Parent;
                }

                if (tmp == null)
                {
                    _out.WriteLine("The successor of node " + key + " was not found.");
                    _out.WriteLine();
                    return;
                }

                _out.WriteLine("The successor of node " + key + " is " + tmp.Key + ".");
                _draw.Mark(tmp);
                var thread = new Thread(() => UnmarkThread(_draw, tmp));
                thread.Start();
                thread.Join();
            }
        }

        public void Predecessor(int key)
        {
            var current = GetNode(key);

            if (current == null)
            {
                _out.WriteLine("Key " + key + " was not found!");
                _out.WriteLine();
                return;
            }

            if (current.Left != null)
            {
                var maximum = current.Left.GetMaximumValueOfSubtree();
                _out.WriteLine("The predecessor of node " + key + " is " + maximum.Key + ".");

                _draw.Mark(maximum);
                var thread = new Thread(() => UnmarkThread(_draw, maximum));
                thread.Start();
                thread.Join();
            }
            else
            {
                var tmp = current.Parent;

                while (tmp != null && current == tmp.Left)
                {
                    current = tmp;
                    tmp = tmp.Parent;
                }

                if (tmp == null)
                {
                    _out.WriteLine("The successor of node " + key + " was not found.");
                    _out.WriteLine();
                    return;
                }

                _out.WriteLine("The predecessor of node " + key + " is " + tmp.Key + ".");

                var minimum = tmp.GetMinimumValueOfSubtree();
                _draw.Mark(minimum);
                var thread = new Thread(() => UnmarkThread(_draw, minimum));
                thread.Start();
                thread.Join();
            }
        }

        public void UpdateNodesLabel()
        {
            _nodesBox.Text = @"Nodes: " + Nodes;
        }

        public override string ToString()
        {
            return "Nodes: " + _nodes + "\nRoot: " + _root;
        }
    }
}